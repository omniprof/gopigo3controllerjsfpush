package com.kenfogel.gopigo3controllerjsfpush;

import com.kenfogel.gopigo3driver.GoPiGo3;

import java.io.Serializable;
import javax.inject.Named;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Backing bean for index.xhtml This class sends messages to the GoPiGo3
 * controller class
 *
 * @author Ken Fogel
 * @version 0.1
 */
@Named("gopigoBacking")
@ApplicationScoped
public class GoPiGoBacking implements Serializable {

    private static final byte MOTOR_LEFT = 1;
    private static final byte MOTOR_RIGHT = 2;

    static final Logger LOG = Logger.getLogger(GoPiGoBacking.class.getName());

    @Inject
    private GoPiGo3 gopigo;

    @Inject
    private PushDistanceBacking pdb;

    @Inject
    private ServoBacking sb;

    // Thread factory for Java EE since version 7
    @Resource
    private ManagedThreadFactory threadFactory;

    // The thread as a class variable so we can have a stop 
    //private Thread ultraThread;
    /**
     * Constructor initializes GoPiGo3 and UltrasonicSensor
     *
     */
    public GoPiGoBacking() {
        super();
    }

    /**
     * Start the ultrasonic sensor
     *
     */
    public void startUltra() {
        LOG.log(Level.INFO, "Start Ultrasonic");
        Thread ultraThread = threadFactory.newThread(pdb);
        ultraThread.start();
    }

    /**
     * Stop the ultrasonic sensor
     */
    public void stopUltra() {
        pdb.stop();
    }

    /**
     * Start the servo
     *
     */
    public void startServo() {
        LOG.log(Level.INFO, "Start Servo");
        Thread ultraThread = threadFactory.newThread(sb);
        ultraThread.start();
    }

    /**
     * Stop the servo
     */
    public void stopServo() {
        sb.stop();
    }

    /**
     * Forward direction
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goForward() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving forward");
        gopigo.runMotor(MOTOR_LEFT, (byte) 50);
        gopigo.runMotor(MOTOR_RIGHT, (byte) 50);
    }

    /**
     * Reverse direction
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goReverse() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving reverse");
        gopigo.runMotor(MOTOR_LEFT, (byte) -50);
        gopigo.runMotor(MOTOR_RIGHT, (byte) -50);
    }

    /**
     * Turn Left
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goLeft() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving left");
        gopigo.stopMotor(MOTOR_LEFT);
        gopigo.runMotor(MOTOR_RIGHT, (byte) 50);
    }

    /**
     * Turn Right
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goRight() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Moving right");
        gopigo.stopMotor(MOTOR_RIGHT);
        gopigo.runMotor(MOTOR_LEFT, (byte) 50);
    }

    /**
     * Stop the motors
     *
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void goStop() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Stopping");
        gopigo.stopMotors();
    }

    /**
     * Turn on both LED eyes
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goTurnOnBothLED() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Turn on both eyes");
        gopigo.doLeftEyeLED((byte) 25, (byte) 25, (byte) 25);
        gopigo.doRightEyeLED((byte) 25, (byte) 25, (byte) 25);
    }

    /**
     * Turn off both LED eyes
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goTurnOffBothLED() throws IOException, InterruptedException {
        LOG.log(Level.INFO, "Turn off both eyes");
        gopigo.doLeftEyeLED((byte) 0, (byte) 0, (byte) 0);
        gopigo.doRightEyeLED((byte) 0, (byte) 0, (byte) 0);
    }

}
