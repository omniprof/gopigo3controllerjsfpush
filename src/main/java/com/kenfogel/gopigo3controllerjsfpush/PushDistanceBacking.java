package com.kenfogel.gopigo3controllerjsfpush;

import com.kenfogel.gopigo3driver.GoPiGo3;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;

/**
 * This is the Runnable thread.
 *
 * @author Ken Fogel
 */
@ApplicationScoped
public class PushDistanceBacking implements Runnable {

    static final Logger LOG = Logger.getLogger(PushDistanceBacking.class.getName());

    // Status of the thread used to ensure that the thread is only started once 
    // and used to stop the thread
    private final AtomicBoolean running = new AtomicBoolean(false);

    // Inject the object that will send data through the socket
    @Inject
    @Push(channel = "distance")
    private PushContext push;

    // The GoPiGo3 driver
    @Inject
    private GoPiGo3 gopigo;

    /**
     * Stop the thread by setting the AtomicBoolean it is watching to false
     */
    public void stop() {
        running.set(false);
    }

    /**
     * The thread run method that continuously reads from the ultrasonic sensor
     *
     */
    @Override
    public void run() {
        LOG.log(Level.INFO, "pushDistance = {0}", running.get());
        if (!running.get()) { // start it only once
            running.set(true);
            int counter = 0;
            // Watch running to determine when the thread must end
            while (running.get()) {
                try {
                    int dist = gopigo.doUltra();
                    // Some basic logic that will stop the car if the ultrasonic 
                    // sensor reports that it is 100 mm from an onstacle
                    if (dist < 100) {
                        counter++;
                        // Require 3 readings of 0 in a row
                        if (counter > 3) {
                            LOG.log(Level.INFO, "Auto Stop");
                            gopigo.stopMotors();
                            counter = 0;
                        }
                    }
                    // Pushes a string
                    push.send("Distance = " + dist);
                } catch (IOException | InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(PushDistanceBacking.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
