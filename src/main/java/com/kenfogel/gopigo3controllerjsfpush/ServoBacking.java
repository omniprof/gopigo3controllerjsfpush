package com.kenfogel.gopigo3controllerjsfpush;

import com.kenfogel.gopigo3driver.GoPiGo3;
import com.kenfogel.gopigo3driver.GoPiGo3Servo;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * This is the Runnable thread.
 *
 * @author Ken Fogel
 */
@ApplicationScoped
public class ServoBacking implements Runnable {

    static final Logger LOG = Logger.getLogger(ServoBacking.class.getName());

    // Status of the thread used to ensure that the thread is only started once 
    // and used to stop the thread
    private final AtomicBoolean running = new AtomicBoolean(false);

    // The GoPiGo3 driver
    @Inject
    private GoPiGo3Servo gopigo;

    /**
     * Stop the thread by setting the AtomicBoolean it is watching to false
     */
    public void stop() {
        running.set(false);
    }

    /**
     * The thread run method that continuously reads from the ultrasonic sensor
     *
     */
    @Override
    public void run() {
        LOG.log(Level.INFO, "Cylon Mode = {0}", running.get());
        if (!running.get()) { // start it only once
            running.set(true);
            int counter = 0;
            // Watch running to determine when the thread must end
            while (running.get()) {
                try {
                    gopigo.set_servo((byte)1);
                } catch (IOException | InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    Logger.getLogger(ServoBacking.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
